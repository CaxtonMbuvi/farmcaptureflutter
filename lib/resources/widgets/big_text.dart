import 'package:flutter/material.dart';

import '../utils/dimensions.dart';

class BigText extends StatelessWidget {
  final Color? color;
  final String text;
  double size;
  TextOverflow overFlow;

  BigText({Key? key, this.color = const Color(0xFFFFFFFF), 
  required this.text,
  this.size = 0,
  this.overFlow = TextOverflow.ellipsis
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
       text,
       maxLines: 1,
       overflow: overFlow,
       style: TextStyle(
        fontFamily: 'SpaceGrotesk',
         color: color,
         fontSize: size==0?Dimensions.font28:size,
         fontWeight: FontWeight.w700
       ),
    );
  }
}
