import 'package:flutter/material.dart';

import '../utils/dimensions.dart';

class SmallText extends StatelessWidget {
  final Color? color;
  final String text;
  double size;
  final TextDecoration? decoration;

  SmallText(
      {Key? key,
      this.color =  Colors.black,
      required this.text,
      this.size = 0,
      this.decoration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: 'SpaceGrotesk',
          color: color,
          fontSize: size == 0 ? Dimensions.font12 : size,
          decoration: decoration),
    );
  }
}
