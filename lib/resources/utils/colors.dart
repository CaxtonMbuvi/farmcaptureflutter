import 'dart:ui';

class AppColors{
  static Color gradientOne = const Color(0xFF6985e8);
  static Color textWhite = const Color(0xFFFFFFFF);
  static Color mainColor = const Color(0xFF4CAF50);
  static Color subColor = Color(0xFF2E7D32);
  static Color white200 = const Color(0xFFE7F4FD);
  static Color blue300 = const Color(0xFFBBE1FA);
  static Color blue200 = const Color(0xFFE7F4FD);
  static Color black = const Color(0xFF000000);
  static Color textGrey = const Color(0xFFFAFAFA);
  static Color buttonWhite = const Color(0xFFFFFFFF);
  static Color buttonGreen = const Color(0xFF64DD17);
  static Color buttonYellow = const Color(0xFFFFAD46);
  static Color buttonRed = const Color(0xFFFF0000);
}