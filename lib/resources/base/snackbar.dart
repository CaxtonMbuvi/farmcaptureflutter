// import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:blobs/blobs.dart';
// import '../widgets/big_text.dart';

// void showCustomSnackBar(String message, context, ContentType type,
//     {bool isError = true, String title = "Error"}) {
 
//   final snackBar = SnackBar(
//     /// need to set following properties for best effect of awesome_snackbar_content
//     elevation: 0,
    
//     behavior: SnackBarBehavior.floating,
//     backgroundColor: Colors.transparent,
//     content: AwesomeSnackbarContent(
//       title: title,
//       message: message,

//       /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
//       contentType: type,
//     ),
//   );

//   ScaffoldMessenger.of(context)
//     ..hideCurrentSnackBar()
//     ..showSnackBar(snackBar);
// }
