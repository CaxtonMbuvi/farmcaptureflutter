import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutterfarmcapture/mainpages/dashboard.dart';
import 'package:flutterfarmcapture/resources/utils/dimensions.dart';
import 'package:get/get.dart';

import '../../resources/utils/colors.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/custom_text_field.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  bool _isObscure = true;
  var passwordController = TextEditingController();
  var userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print(Dimensions.screenHeight);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: Dimensions.screenWidth,
          height: Dimensions.screenHeight,
          padding: EdgeInsets.only(
            left: Dimensions.width15,
            right: Dimensions.width15,
          ),
          decoration: BoxDecoration(
            color: AppColors.mainColor.withOpacity(1),
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstATop),
              image: AssetImage("assets/images/crops.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: Dimensions.height30,
              ),
              Center(
                child: Column(
                  children: [
                    BigText(
                      text: "Welcome to",
                      color: AppColors.textWhite,
                      size: Dimensions.font16,
                    ),
                    SizedBox(
                      height: Dimensions.height30,
                    ),
                    Image.asset("assets/images/fc_logo.png",
                        fit: BoxFit.contain, height: Dimensions.height45),
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height45 * 3,
              ),
              BigText(
                text: "UserName",
                color: AppColors.textWhite,
                size: Dimensions.font14,
              ),
              CustomTextField(
                readOnly: false,
                controller: userNameController,
                hintText: "Username",
                isObscure: false,
                onSaved: (value) {},
                onTap: (value) {},
              ),
              SizedBox(
                height: Dimensions.height30,
              ),
              BigText(
                text: "Password",
                color: AppColors.textWhite,
                size: Dimensions.font14,
              ),
              SizedBox(
                child: TextFormField(
                  controller: passwordController,
                  obscureText: _isObscure,
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    focusColor: Theme.of(context).primaryColor,
                    hintText: "Password",
                    contentPadding: EdgeInsets.only(top: 15, left: 5),
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                      child: Icon(
                        Icons.visibility,
                        color: Colors.green,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 2,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  child: Container(
                    width: Dimensions.width30 * Dimensions.width10,
                    padding: EdgeInsets.only(
                      top: Dimensions.height15,
                      bottom: Dimensions.height15,
                      left: Dimensions.width30,
                      right: Dimensions.width30,
                    ),
                    decoration: BoxDecoration(
                        color: AppColors.textWhite,
                        borderRadius:
                            BorderRadius.circular(Dimensions.radius20)),
                    child: Center(
                      child: BigText(
                        text: "SIGN IN",
                        size: Dimensions.font18,
                        color: AppColors.black,
                      )
                    ),
                  ),
                  onTap: () {
                    Get.to(() => const DashboardScreen());
                  },
                ),
              ),
              SizedBox(
                height: Dimensions.height30*2,
              ),
              // Center(
              //   child: RichText(
              //     text: TextSpan(
              //       text: 'Don\'t have an Account? ',
              //       style: TextStyle(
              //         color: AppColors.textWhite,
              //         fontSize: Dimensions.font14,
              //       ),
              //       children: const <TextSpan>[
              //         TextSpan(
              //           text: 'REGISTER NOW',
              //           style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               color: Colors.yellowAccent),
              //         ),
              //       ],
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
