import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutterfarmcapture/mainpages/farmer/farmer_dashboard.dart';
import 'package:flutterfarmcapture/mainpages/outgrowers/outgrowers_dashboard.dart';
import 'package:flutterfarmcapture/resources/utils/colors.dart';
import 'package:flutterfarmcapture/resources/utils/dimensions.dart';
import 'package:flutterfarmcapture/resources/widgets/small_text.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../resources/widgets/big_text.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          width: Dimensions.screenWidth,
          height: Dimensions.screenHeight,
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height30 * Dimensions.height10,
                child: Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: Dimensions.height30 * 2,
                          left: Dimensions.width20,
                          right: Dimensions.width20),
                      height: Dimensions.height15 * (Dimensions.height20 - 3),
                      decoration: BoxDecoration(
                          color: AppColors.mainColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft:
                                Radius.circular(Dimensions.radius30 * 4),
                            bottomRight:
                                Radius.circular(Dimensions.radius30 * 4),
                          )),
                      child: SizedBox(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SmallText(
                                  text: "Hello,  Admin",
                                  size: Dimensions.font20,
                                  color: AppColors.textWhite,
                                ),
                                Container(
                                  height: Dimensions.height20 * 3,
                                  // width: Dimensions.width20 * 7.5,
                                  decoration: BoxDecoration(
                                      color: AppColors.subColor,
                                      borderRadius: BorderRadius.circular(
                                          Dimensions.radius30)),
                                  child: Center(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        top: Dimensions.height10 / 3,
                                        bottom: Dimensions.height10 / 3,
                                        left: Dimensions.width10 / 2,
                                        right: Dimensions.width10 / 2,
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          FaIcon(
                                            FontAwesomeIcons.rotate,
                                            size: Dimensions.height30,
                                            color: AppColors.textWhite,
                                          ),
                                          SizedBox(
                                            width: Dimensions.width20,
                                          ),
                                          SmallText(
                                            text: "SYNC(0)",
                                            size: Dimensions.font20,
                                            color: AppColors.textWhite,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: Dimensions.height20,
                            ),
                            Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  FaIcon(
                                    FontAwesomeIcons.circleInfo,
                                    size: Dimensions.height10,
                                    color: AppColors.buttonRed,
                                  ),
                                  SizedBox(
                                    width: Dimensions.width10,
                                  ),
                                  SmallText(
                                    text: "Kindly SYNC before start",
                                    size: Dimensions.font14,
                                    color: Colors.black,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    
                    Positioned(
                      top: Dimensions.height20 * Dimensions.height10,
                      left: Dimensions.width20 * (Dimensions.width10 / 1.9),
                      child: Container(
                        height: Dimensions.height20 * 5,
                        width: Dimensions.width20 * Dimensions.width15,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(Dimensions.radius20)),
                        child: Center(
                          child: BigText(
                            text: "Dashboard",
                            size: Dimensions.font20,
                            color: AppColors.black,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: Dimensions.height30,
                  left: Dimensions.width30 * 2,
                  right: Dimensions.width30 * 2,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.to(() => const FarmerDashboardScreen());
                              },
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_1.png',
                                        width: 60,
                                        height: 60,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Farmers' Profiling",
                              color: AppColors.black,
                              size: Dimensions.font14,
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.to(() => const OutGrowersDashboardScreen());
                              },
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/outgrowers.png',
                                        width: 50,
                                        height: 50,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "OutGrowers",
                              color: AppColors.black,
                              size: Dimensions.font14,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height45 + 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            GestureDetector(
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_3.png',
                                        width: 60,
                                        height: 60,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Estate Management",
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_4.png',
                                        width: 50,
                                        height: 50,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: 'Visit',
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height45 + 5,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
