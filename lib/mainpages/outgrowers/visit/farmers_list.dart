import 'package:flutter/material.dart';
import 'package:flutterfarmcapture/mainpages/outgrowers/outgrowers_dashboard.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class VisitFarmerListScreen extends StatefulWidget {
  const VisitFarmerListScreen({super.key});

  @override
  State<VisitFarmerListScreen> createState() => _VisitFarmerListScreenState();
}

class _VisitFarmerListScreenState extends State<VisitFarmerListScreen> {
  final List<String> items =
      List<String>.generate(20, (index) => "Item $index");
  final List<Map<String, dynamic>> _allUsers = [
    {"id": 1, "name": "Andy", "age": 29},
    {"id": 2, "name": "Aragon", "age": 40},
    {"id": 3, "name": "Bob", "age": 5},
    {"id": 4, "name": "Barbara", "age": 35},
    {"id": 5, "name": "Candy", "age": 21},
    {"id": 6, "name": "Colin", "age": 55},
    {"id": 7, "name": "Audra", "age": 30},
    {"id": 8, "name": "Banana", "age": 14},
    {"id": 9, "name": "Caversky", "age": 100},
    {"id": 10, "name": "Becky", "age": 32},
  ];
  List<String>? filteredItems;
  List<Map<String, dynamic>>? filteredItems2;
  TextEditingController _controller = TextEditingController();
  TextEditingController _controller2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    filteredItems = items;
    filteredItems2 = _allUsers;
    _controller.addListener(() {
      setState(() {
        filteredItems = items
            .where((item) =>
                item.toLowerCase().contains(_controller.text.toLowerCase()))
            .toList();
      });
    });

    _controller2.addListener(() {
      setState(() {
        filteredItems2 = _allUsers
            .where((item) =>
                item['name']
                    .toString()
                    .toLowerCase()
                    .contains(_controller2.text.toLowerCase()) ||
                item['age']
                    .toString()
                    .toLowerCase()
                    .contains(_controller2.text.toLowerCase()))
            .toList();
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _controller2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const OutGrowersDashboardScreen());
                      },
                    ),
                    BigText(
                      text: "Farmers' List",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        width: Dimensions.screenWidth,
        height: Dimensions.screenHeight,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: Dimensions.height30,
            ),
            SizedBox(
              height: Dimensions.height30 * 3,
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: Dimensions.screenWidth / 1.3,
                        padding: EdgeInsets.only(
                          top: Dimensions.height10,
                          bottom: Dimensions.height10,
                        ),
                        child: Center(
                          child: TextField(
                            controller: _controller2,
                            autocorrect: true,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Search Farmer",
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: BorderSide(
                                    color: AppColors.textGrey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(Dimensions.radius30)),
                                borderSide: BorderSide(
                                    color: AppColors.mainColor, width: 2),
                              ),
                            ),
                          ),
                        )),
                    Flexible(
                      child: Container(
                        width: Dimensions.width30 * 2,
                        height: Dimensions.height30 * 2,
                        padding: EdgeInsets.only(
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.magnifyingGlass,
                            size: Dimensions.height20,
                            color: AppColors.black,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: Dimensions.width10,
                                  right: Dimensions.width10,
                                  top: Dimensions.height10),
                              child: GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    height: Dimensions.height30 * 4,
                                    width: Dimensions.screenWidth,
                                    decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        borderRadius: BorderRadius.circular(
                                            Dimensions.radius20)),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: Dimensions.width20,
                                        right: Dimensions.width15,
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          BigText(
                                            text:
                                                "Name: ${filteredItems2![index]['name']}",
                                            color: AppColors.mainColor,
                                            size: Dimensions.font16,
                                          ),
                                          BigText(
                                            text:
                                                "Nat ID: ${filteredItems2![index]['age']}",
                                            color: AppColors.black,
                                            size: Dimensions.font16,
                                          ),
                                          BigText(
                                            text:
                                                "Phone: ${filteredItems2![index]['name']}",
                                            color: AppColors.black,
                                            size: Dimensions.font16,
                                          ),
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                            if (index != filteredItems2!.length - 1) Divider(),
                          ],
                        );
                      },
                      childCount: filteredItems2!.length,
                      addAutomaticKeepAlives: false,
                      addRepaintBoundaries: false,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
