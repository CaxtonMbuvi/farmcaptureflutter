import 'package:flutter/material.dart';
import 'package:flutterfarmcapture/mainpages/dashboard.dart';
import 'package:flutterfarmcapture/mainpages/outgrowers/visit/farmers_list.dart';
import 'package:get/get.dart';

import '../../resources/utils/colors.dart';
import '../../resources/utils/dimensions.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/small_text.dart';

class OutGrowersDashboardScreen extends StatefulWidget {
  const OutGrowersDashboardScreen({super.key});

  @override
  State<OutGrowersDashboardScreen> createState() =>
      _OutGrowersDashboardScreenState();
}

class _OutGrowersDashboardScreenState extends State<OutGrowersDashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.to(() => const DashboardScreen());
                      },
                    ),
                    BigText(
                      text: "Outgrowers Dashboard",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height45,
              ),
              GestureDetector(
                onTap: () {
                  Get.to(() => const VisitFarmerListScreen());
                },
                child: Column(
                  children: [
                    Container(
                      height: Dimensions.height30 * 4,
                      width: Dimensions.width30 * 4,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius:
                            BorderRadius.circular(Dimensions.radius30 * 5),
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/ic_farmvisits.png',
                              width: Dimensions.width30 * 2,
                              height: Dimensions.height30 * 2,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    BigText(
                      text: "VISITS",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    SizedBox(
                        width: Dimensions.width45 + Dimensions.width45,
                        child: const Divider()),
                    SmallText(
                      text: "Visitations",
                      color: Colors.blueGrey,
                      size: Dimensions.font16 - 4,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 2,
              ),
              GestureDetector(
                onTap: () {},
                child: Column(
                  children: [
                    GestureDetector(
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius30 * 5),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/ic_grouptraining.png',
                                width: Dimensions.width30 * 2,
                                height: Dimensions.height30 * 2,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    BigText(
                      text: "GROUP TRAININGS",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    SizedBox(
                        width: Dimensions.width45 + Dimensions.width45,
                        child: const Divider()),
                    SmallText(
                      text: "Group Trainings",
                      color: Colors.blueGrey,
                      size: Dimensions.font16 - 4,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 2,
              ),
              GestureDetector(
                onTap: () {},
                child: Column(
                  children: [
                    GestureDetector(
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius30 * 5),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/ic_farmer_additional_info.png',
                                width: Dimensions.width30 * 2,
                                height: Dimensions.height30 * 2,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    BigText(
                      text: "FARMERS REGISTRATION",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                    SizedBox(
                      height: Dimensions.height10 / 2,
                    ),
                    SizedBox(
                      width: Dimensions.width45 + Dimensions.width45,
                      child: const Divider(),
                    ),
                    SmallText(
                      text: "Add New Farmer",
                      color: Colors.blueGrey,
                      size: Dimensions.font16 - 4,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
