
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';

import '../../resources/base/custom_loader.dart';
import '../../resources/utils/colors.dart';
import '../../resources/utils/dimensions.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/custom_text_field.dart';
import '../../resources/widgets/small_text.dart';

class EditFarmer extends StatefulWidget {
  const EditFarmer({super.key});

  @override
  State<EditFarmer> createState() => _EditFarmerState();
}

class _EditFarmerState extends State<EditFarmer> {
  var pageId = Get.arguments;
  bool isLoading = false;
  File? image;

  var farmerNameController = TextEditingController();
  var firstNameController = TextEditingController();
  var middleNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var otherNameController = TextEditingController();
  var maritalStatusController = TextEditingController();
  var phoneNoController = TextEditingController();
  var genderController = TextEditingController();
  var nationalIdController = TextEditingController();
  var dobController = TextEditingController();
  var bankIdController = TextEditingController();
  var bankBranchIdController = TextEditingController();
  var accNoController = TextEditingController();
  var passportController = TextEditingController();
  var photoController = TextEditingController();

  var regionIdController = TextEditingController();
  var districtIdController = TextEditingController();
  var wardIdController = TextEditingController();
  var villageIdController = TextEditingController();
  var colCenterController = TextEditingController();
  var nationalityController = TextEditingController();

  var typeController = TextEditingController();

  var farmerDeductionController = TextEditingController();

  
  //Drop Down Lists Here
  // List<BankResultElement> _banks = [];
  // List<BankBranchResultElement> _bankBranches = [];

  // List<GenderResultElement> _genderTypes = [];
  // List<MaritalStatusResultElement> _maritalStatus = [];
  // List<CountyResultElement> _counties = [];
  // List<DistrictResultElement> _districts = [];
  // List<WardResultElement> _ward = [];
  // List<VillageResultElement> _village = [];
  // List<CountyResultElement> _colCenter1 = [];
  // List<NationalityResultElement> _nationalities = [];
  // List<ResultElementDeduct> _deductions = [];

    /**for date picker**/
  DateTime date = DateTime.now();
  Future<Null> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        dobController.text = formattedDate;

        print(date.toString());
      });
    }
  }

    Future _getImage() async {
    final picker = ImagePicker();

    var _pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    if (_pickedFile != null) {
      final bytes = File(_pickedFile.path).readAsBytesSync();
      String img64 = base64Encode(bytes);

      setState(() {
        photoController.text = img64;
        image = File(_pickedFile.path);
      });
    }
    //String filename = (image.split("/").last).toString();
    print(image);
    print(photoController.text.toString());
    //return image;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: Dimensions.iconSize24,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                  BigText(
                    text: "Edit Farmer Profile",
                    color: AppColors.textWhite,
                    size: Dimensions.font20,
                  ),
                  SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: isLoading
              ? CustomLoader()
              : Container(
                  color: AppColors.textWhite,
                  width: Dimensions.screenWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: Dimensions.height15,
                      ),
                      Container(
                        width: Dimensions.screenWidth,
                        padding: EdgeInsets.only(
                          top: Dimensions.height45,
                          left: Dimensions.width15,
                          right: Dimensions.width15,
                          bottom: Dimensions.height15,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: BigText(
                                text: "Personal Details",
                                color: AppColors.mainColor,
                                size: Dimensions.font20,
                              ),
                            ),

                            Container(
                              child: Column(
                                children: [
                                  CustomTextField(
                                    readOnly: false,
                                    controller: firstNameController,
                                    onSaved: (value) {},
                                    labelText: "Full Name",
                                    isObscure: false,
                                    onTap: (value) {},
                                  ),

                                  CustomTextField(
                                    readOnly: false,
                                    controller: phoneNoController,
                                    onSaved: (value) {},
                                    labelText: "Phone No",
                                    isObscure: false,
                                    onTap: (value) {},
                                  ),

                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<
                                  //       MaritalStatusResultElement>(
                                  //     hint: Text("Marital Status"),
                                  //     value: selectedMaritalStatus,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (MaritalStatusResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedMaritalStatus = newValue;
                                  //         maritalStatusController.text =
                                  //             selectedMaritalStatus!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _maritalStatus.map(
                                  //         (MaritalStatusResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           MaritalStatusResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<GenderResultElement>(
                                  //     hint: Text("Gender"),
                                  //     value: selectedGender,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (GenderResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedGender = newValue;
                                  //         genderController.text =
                                  //             selectedGender!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _genderTypes
                                  //         .map((GenderResultElement gender) {
                                  //       return DropdownMenuItem<
                                  //           GenderResultElement>(
                                  //         value: gender,
                                  //         child: Text(gender.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  //dob
                                  Container(
                                    margin: const EdgeInsets.all(10.0),
                                    child: TextFormField(
                                      onTap: () {
                                        selectedTimePicker(context);
                                      },
                                      controller: dobController,
                                      cursorColor: Colors.black,
                                      keyboardType: TextInputType.text,
                                      readOnly: true,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        focusColor: AppColors.mainColor,
                                        //hintText: "Date Of Birth",
                                        contentPadding:
                                            EdgeInsets.only(left: 5),
                                        suffixIcon: Icon(Icons.calendar_month),
                                        labelText: "Date Of Birth",
                                      ),
                                    ),
                                  ),

                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<BankResultElement>(
                                  //     hint: Text("Bank"),
                                  //     value: selectedBank,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged:
                                  //         (BankResultElement? newValue) async {
                                  //       setState(() {
                                  //         selectedBank = newValue;
                                  //         bankIdController.text =
                                  //             selectedBank!.resultId.toString();
                                  //         print(
                                  //             "Here is Bank ==*** ${selectedBank!.name.toString()} and id === ${selectedBank!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items:
                                  //         _banks.map((BankResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           BankResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child:
                                  //       DropdownButton<BankBranchResultElement>(
                                  //     hint: Text("Bank Branch"),
                                  //     value: selectedBankBranch,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (BankBranchResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedBankBranch = newValue;
                                  //         bankBranchIdController.text =
                                  //             selectedBankBranch!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is Bank Branch ==*** ${selectedBankBranch!.name.toString()} and id === ${selectedBankBranch!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _bankBranches
                                  //         .map((BankBranchResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           BankBranchResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height15,
                                  ),

                                  CustomTextField(
                                    readOnly: false,
                                    controller: accNoController,
                                    onSaved: (value) {},
                                    labelText: "Account Number",
                                    isObscure: false,
                                    onTap: (value) {},
                                  ),
                                  SizedBox(
                                    height: Dimensions.height10,
                                  ),
                                  Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: Dimensions.height10 *
                                              Dimensions.height20,
                                          width: Dimensions.screenWidth / 2,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      Dimensions.radius20 / 2),
                                              color: AppColors.textGrey),
                                          child: image != null
                                              ? ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Dimensions.radius15),
                                                  child: Image.file(image!,
                                                      fit: BoxFit.cover),
                                                )
                                              : const Center(
                                                  child: Text(
                                                      'Please add a photo'),
                                                ),
                                        ),
                                        SizedBox(
                                          width: Dimensions.width30,
                                        ),
                                        MaterialButton(
                                          color: Colors.blue,
                                          onPressed: _getImage,
                                          child: Text(
                                            "Take a Photo",
                                            style: TextStyle(
                                                color: AppColors.textWhite,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            SizedBox(
                              height: Dimensions.height45,
                            ),

                            // LOCATION DETAILS
                            SizedBox(
                              height: Dimensions.height30,
                            ),

                            Center(
                              child: BigText(
                                text: "Location Details",
                                color: AppColors.mainColor,
                                size: Dimensions.font20,
                              ),
                            ),

                            Container(
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: Dimensions.height30,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<CountyResultElement>(
                                  //     hint: Text("County"),
                                  //     value: selectedCounty,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (CountyResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedCounty = newValue;
                                  //         regionIdController.text =
                                  //             selectedCounty!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is County ==*** ${selectedCounty!.name.toString()} and id === ${selectedCounty!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _counties
                                  //         .map((CountyResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           CountyResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height20,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child:
                                  //       DropdownButton<DistrictResultElement>(
                                  //     hint: Text("District"),
                                  //     value: selectedDistrict,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (DistrictResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedDistrict = newValue;
                                  //         districtIdController.text =
                                  //             selectedDistrict!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is District ==*** ${selectedDistrict!.name.toString()} and id === ${selectedDistrict!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _districts
                                  //         .map((DistrictResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           DistrictResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height20,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<WardResultElement>(
                                  //     hint: Text("Town"),
                                  //     value: selectedWard,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged:
                                  //         (WardResultElement? newValue) async {
                                  //       setState(() {
                                  //         selectedWard = newValue;
                                  //         wardIdController.text =
                                  //             selectedWard!.resultId.toString();
                                  //         print(
                                  //             "Here is Ward ==*** ${selectedWard!.name.toString()} and id === ${selectedWard!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items:
                                  //         _ward.map((WardResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           WardResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height20,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<VillageResultElement>(
                                  //     hint: Text("Village"),
                                  //     value: selectedVillage,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (VillageResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedVillage = newValue;
                                  //         villageIdController.text =
                                  //             selectedVillage!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is Village ==*** ${selectedVillage!.name.toString()} and id === ${selectedVillage!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _village
                                  //         .map((VillageResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           VillageResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height20,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<String>(
                                  //     hint: Text("Collection Center"),
                                  //     value: _colCenterId,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (newValue) async {
                                  //       setState(() {
                                  //         _colCenterId = newValue;
                                  //         //colCenterController.text =
                                  //       });
                                  //     },
                                  //     items: _allItems.map((value) {
                                  //       return DropdownMenuItem<String>(
                                  //         value: value['name'].trim(),
                                  //         child: Text(value['name'].trim(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),

                                  SizedBox(
                                    height: Dimensions.height20,
                                  ),

                                  // Container(
                                  //   margin: const EdgeInsets.all(10.0),
                                  //   child: DropdownButton<
                                  //       NationalityResultElement>(
                                  //     hint: Text("Nationality"),
                                  //     value: selectedNationality,
                                  //     isExpanded: true,
                                  //     isDense: true,
                                  //     dropdownColor: Colors.white,
                                  //     icon: Icon(Icons.arrow_drop_down),
                                  //     style: TextStyle(
                                  //         color: Colors.black, fontSize: 16),
                                  //     onChanged: (NationalityResultElement?
                                  //         newValue) async {
                                  //       setState(() {
                                  //         selectedNationality = newValue;
                                  //         nationalityController.text =
                                  //             selectedNationality!.resultId
                                  //                 .toString();
                                  //         print(
                                  //             "Here is Nationality ==*** ${selectedNationality!.name.toString()} and id === ${selectedNationality!.resultId.toString()}");
                                  //       });
                                  //     },
                                  //     items: _nationalities.map(
                                  //         (NationalityResultElement value) {
                                  //       return DropdownMenuItem<
                                  //           NationalityResultElement>(
                                  //         value: value,
                                  //         child: Text(value.name.toString(),
                                  //             style: TextStyle(
                                  //                 color: Colors.black)),
                                  //       );
                                  //     }).toList(),
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      Center(
                        child: BigText(
                          text: "Farmer Deductions",
                          color: AppColors.mainColor,
                          size: Dimensions.font20,
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            //open add family dialog
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                child: Container(
                                  width: Dimensions.width20 *
                                      (Dimensions.width10 / 2),
                                  margin: EdgeInsets.only(
                                      right: Dimensions.width10),
                                  padding: EdgeInsets.only(
                                    top: Dimensions.height15,
                                    bottom: Dimensions.height15,
                                    left: Dimensions.width30,
                                    right: Dimensions.width30,
                                  ),
                                  decoration: BoxDecoration(
                                    color: AppColors.subColor,
                                    borderRadius: BorderRadius.circular(
                                        Dimensions.radius15 / 3),
                                  ),
                                  child: Center(
                                    child: SmallText(
                                      text: "+ ADD",
                                      color: AppColors.textWhite,
                                      size: Dimensions.font12,
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  openFarmerDeductionsDialog();
                                },
                              ),
                            ),

                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.all(10),
                              decoration: new BoxDecoration(
                                borderRadius:
                                    new BorderRadius.circular(5.0),
                                color: Colors.grey,
                              ),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Cultivated Crops",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Column(
                                    children: [
                                      ListView.builder(
                                        physics:
                                            NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: 1,
                                        itemBuilder: (context, index) {
                                          return ListTile(
                                            title: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    Text(
                                                      "NAME: ",
                                                      style: TextStyle(
                                                          color: Colors
                                                              .black,
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight
                                                                  .normal),
                                                    ),
                                                  ],
                                                ),
                                                Divider(
                                                  height: 0.5,
                                                  color: Colors.black,
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            
                          },
                          child: Container(
                            height: Dimensions.height30 * 2,
                            width: Dimensions.width30 * Dimensions.width10,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.circular(Dimensions.radius30),
                            ),
                            child: Center(
                              child: BigText(
                                text: "Submit",
                                color: AppColors.textWhite,
                                size: Dimensions.font16,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Dimensions.height30,
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  /**crops cultivated in area dialog**/
  openFarmerDeductionsDialog() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SimpleDialog(
                title: BigText(
                  text: "Add Farmer Deduction",
                  color: AppColors.black,
                  size: Dimensions.font16,
                ),
                children: [
                  SimpleDialogOption(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      // child: Container(
                      //   width: Dimensions.screenWidth,
                      //   child: DropdownButton<ResultElementDeduct>(
                      //     hint: Text("Deduction"),
                      //     value: selectedDeductions,
                      //     isExpanded: true,
                      //     isDense: true,
                      //     dropdownColor: Colors.white,
                      //     icon: Icon(Icons.arrow_drop_down),
                      //     style: TextStyle(color: Colors.black, fontSize: 16),
                      //     onChanged: (ResultElementDeduct? newValue) async {
                      //       setState(() {
                      //         selectedDeductions = newValue;
                      //         print(
                      //             "Here is Deduction ==*** ${selectedDeductions!.name.toString()} and id === ${selectedDeductions!.resultId.toString()}");
                      //       });
                      //     },
                      //     items: _deductions.map((ResultElementDeduct value) {
                      //       return DropdownMenuItem<ResultElementDeduct>(
                      //         value: value,
                      //         child: Text(value.name.toString(),
                      //             style: TextStyle(color: Colors.black)),
                      //       );
                      //     }).toList(),
                      //   ),
                      // ),
                    ),
                  ),
                  SimpleDialogOption(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          child: Container(
                            width: 100,
                            padding: EdgeInsets.only(
                                top: 13, bottom: 13, left: 25, right: 25),
                            decoration: BoxDecoration(
                                color: AppColors.subColor,
                                borderRadius: BorderRadius.circular(25)),
                            child: Center(
                              child: Text("save".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.white)),
                            ),
                          ),
                          onTap: () {
                            //_addDeduction();
                          },
                        ),
                        GestureDetector(
                          child: Container(
                            width: 100,
                            padding: EdgeInsets.only(
                                top: 13, bottom: 13, left: 25, right: 25),
                            decoration: BoxDecoration(
                                color: Colors.redAccent,
                                borderRadius: BorderRadius.circular(25)),
                            child: Center(
                              child: Text("cancel".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.white)),
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              );
            },
          );
        });
  }
}