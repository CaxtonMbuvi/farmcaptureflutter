import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../resources/base/custom_loader.dart';
import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/custom_text_field.dart';

class NextOfKinScreen extends StatefulWidget {
  const NextOfKinScreen({super.key});

  @override
  State<NextOfKinScreen> createState() => _NextOfKinScreenState();
}

class _NextOfKinScreenState extends State<NextOfKinScreen> {
  //var pageId = Get.arguments;
  bool _isLoading = false;
  var farmerNameController = TextEditingController();
  var farmerMemberNoController = TextEditingController();

  var kinbirthCertController = TextEditingController();
  var kinfullNameController = TextEditingController();
  var kindobController = TextEditingController();
  var kinisSchooledController = TextEditingController();
  var kinrelationshipIdController = TextEditingController();
  var kinMaritalStatusController = TextEditingController();
  var kinphoneNoController = TextEditingController();
  var kinageController = TextEditingController();
  var kinGenderTypeController = TextEditingController();
  var kinLevelOfEducationController = TextEditingController();
  var kinisOrphanedController = TextEditingController();

//   NextOfKinModel kinModel = NextOfKinModel();

//   //Drop Down Lists Here
//   List<LevelOfEducationResultElement> _educationLevels = [];
//   List<RelationResultElement> _relationLevels = [];
//   List<GenderResultElement> _genderTypes = [];
//   List<MaritalStatusResultElement> _maritalStatus = [];
//   List<CountyResultElement> _colCenter1 = [];

// // Selected Dropdown elements

//   GenderResultElement? selectedGender;
//   MaritalStatusResultElement? selectedMaritalStatus;
//   LevelOfEducationResultElement? selectedLevelOfEducation;
//   RelationResultElement? selectedRelationship;

  /**for date picker**/
  DateTime date = DateTime.now();
  Future<Null> selectedTimePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1960),
        lastDate: DateTime(2024));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        String formattedDate = DateFormat('yyyy-MM-dd').format(picked);
        kindobController.text = formattedDate;

        print(date.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    BigText(
                      text: "Additional Info",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: 
        _isLoading
        ? 
        SizedBox(
          height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const[
              CustomLoader(),
            ],
          ),
        )
        :
        Container(
          color: AppColors.textWhite,
          width: Dimensions.screenWidth,
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height30,
              ),
              BigText(
                text: "Next Of Kin",
                color: AppColors.mainColor,
                size: Dimensions.font20,
              ),
              SizedBox(
                height: Dimensions.height30,
              ),

              CustomTextField(
                readOnly: false,
                controller: kinfullNameController,
                onSaved: (value) {},
                labelText: "Name",
                isObscure: false,
                onTap: (value) {},
              ),

              CustomTextField(
                readOnly: false,
                controller: kinphoneNoController,
                onSaved: (value) {},
                labelText: "Phone Number",
                isObscure: false,
                onTap: (value) {},
              ),

              //dob
              Container(
                margin: const EdgeInsets.all(10.0),
                child: TextFormField(
                  onTap: () {
                    selectedTimePicker(context);
                  },
                  controller: kindobController,
                  cursorColor: Colors.black,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    focusColor: AppColors.mainColor,
                    //hintText: "Date Of Birth",
                    contentPadding: EdgeInsets.only(left: 5),
                    suffixIcon: Icon(Icons.calendar_month),
                    labelText: "Date Of Birth",
                  ),
                ),
              ),

              CustomTextField(
                readOnly: false,
                controller: kinbirthCertController,
                onSaved: (value) {},
                labelText: "Birth Certificate Number",
                isObscure: false,
                onTap: (value) {},
              ),
              SizedBox(
                height: Dimensions.height15,
              ),

              // Container(
              //   margin: const EdgeInsets.all(10.0),
              //   child: DropdownButton<GenderResultElement>(
              //     hint: Text("Gender"),
              //     value: selectedGender,
              //     isExpanded: true,
              //     isDense: true,
              //     dropdownColor: Colors.white,
              //     icon: Icon(Icons.arrow_drop_down),
              //     style: TextStyle(color: Colors.black, fontSize: 16),
              //     onChanged: (GenderResultElement? newValue) async {
              //       setState(() {
              //         selectedGender = newValue;
              //         kinGenderTypeController.text =
              //             selectedGender!.resultId.toString();
              //         print(
              //             "Here is Gender ==*** ${selectedGender!.name.toString()} and id === ${selectedGender!.resultId.toString()}");
              //       });
              //     },
              //     items: _genderTypes.map((GenderResultElement gender) {
              //       return DropdownMenuItem<GenderResultElement>(
              //         value: gender,
              //         child: Text(gender.name.toString(),
              //             style: TextStyle(color: Colors.black)),
              //       );
              //     }).toList(),
              //   ),
              // ),

              SizedBox(
                height: Dimensions.height15,
              ),

              // Container(
              //   margin: const EdgeInsets.all(10.0),
              //   child: DropdownButton<LevelOfEducationResultElement>(
              //     hint: Text("Level Of Education"),
              //     value: selectedLevelOfEducation,
              //     isExpanded: true,
              //     isDense: true,
              //     dropdownColor: Colors.white,
              //     icon: Icon(Icons.arrow_drop_down),
              //     style: TextStyle(color: Colors.black, fontSize: 16),
              //     onChanged:
              //         (LevelOfEducationResultElement? newValue) async {
              //       setState(() {
              //         selectedLevelOfEducation = newValue;
              //         kinLevelOfEducationController.text =
              //             selectedLevelOfEducation!.resultId.toString();
              //         print(
              //             "Here is LevelOfEducation ==*** ${selectedLevelOfEducation!.name.toString()} and id === ${selectedLevelOfEducation!.resultId.toString()}");
              //       });
              //     },
              //     items: _educationLevels
              //         .map((LevelOfEducationResultElement value) {
              //       return DropdownMenuItem<
              //           LevelOfEducationResultElement>(
              //         value: value,
              //         child: Text(value.name.toString(),
              //             style: TextStyle(color: Colors.black)),
              //       );
              //     }).toList(),
              //   ),
              // ),
              SizedBox(
                height: Dimensions.height15,
              ),

              // Container(
              //   margin: const EdgeInsets.all(10.0),
              //   child: DropdownButton<RelationResultElement>(
              //     hint: Text("Relationship"),
              //     value: selectedRelationship,
              //     isExpanded: true,
              //     isDense: true,
              //     dropdownColor: Colors.white,
              //     icon: Icon(Icons.arrow_drop_down),
              //     style: TextStyle(color: Colors.black, fontSize: 16),
              //     onChanged: (RelationResultElement? newValue) async {
              //       setState(() {
              //         selectedRelationship = newValue;
              //         kinrelationshipIdController.text =
              //             selectedRelationship!.resultId.toString();
              //         print(
              //             "Here is Relationship ==*** ${selectedRelationship!.name.toString()} and id === ${selectedRelationship!.resultId.toString()}");
              //       });
              //     },
              //     items: _relationLevels
              //         .map((RelationResultElement value) {
              //       return DropdownMenuItem<RelationResultElement>(
              //         value: value,
              //         child: Text(value.name.toString(),
              //             style: TextStyle(color: Colors.black)),
              //       );
              //     }).toList(),
              //   ),
              // ),

              SizedBox(
                height: Dimensions.height15,
              ),

              // Container(
              //   margin: const EdgeInsets.all(10.0),
              //   child: DropdownButton<MaritalStatusResultElement>(
              //     hint: Text("Marital Status"),
              //     value: selectedMaritalStatus,
              //     isExpanded: true,
              //     isDense: true,
              //     dropdownColor: Colors.white,
              //     icon: Icon(Icons.arrow_drop_down),
              //     style: TextStyle(color: Colors.black, fontSize: 16),
              //     onChanged:
              //         (MaritalStatusResultElement? newValue) async {
              //       setState(() {
              //         selectedMaritalStatus = newValue;
              //         kinMaritalStatusController.text =
              //             selectedMaritalStatus!.resultId.toString();
              //         print(
              //             "Here is Marital status ==*** ${selectedMaritalStatus!.name.toString()} and id === ${selectedMaritalStatus!.resultId.toString()}");
              //       });
              //     },
              //     items: _maritalStatus
              //         .map((MaritalStatusResultElement value) {
              //       return DropdownMenuItem<MaritalStatusResultElement>(
              //         value: value,
              //         child: Text(value.name.toString(),
              //             style: TextStyle(color: Colors.black)),
              //       );
              //     }).toList(),
              //   ),
              // ),

              SizedBox(
                height: Dimensions.height30,
              ),

              GestureDetector(
                onTap: () {
                  
                },
                child: Container(
                  height: Dimensions.height30 * 2,
                  width: Dimensions.width30 * Dimensions.width10,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius:
                        BorderRadius.circular(Dimensions.radius30),
                  ),
                  child: Center(
                    child: BigText(
                      text: "Submit",
                      color: AppColors.textWhite,
                      size: Dimensions.font16,
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: Dimensions.height30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
