import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutterfarmcapture/mainpages/farmer/additional_info/nextofkin.dart';
import 'package:get/get.dart';

import '../../../resources/utils/colors.dart';
import '../../../resources/utils/dimensions.dart';
import '../../../resources/widgets/big_text.dart';
import '../../../resources/widgets/small_text.dart';

class AdditionalInfoScreen extends StatefulWidget {
  const AdditionalInfoScreen({super.key});

  @override
  State<AdditionalInfoScreen> createState() => _AdditionalInfoScreenState();
}

class _AdditionalInfoScreenState extends State<AdditionalInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    BigText(
                      text: "Additional Info Dashboard",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: Dimensions.screenHeight,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height30,
              ),
              Expanded(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: Column(
                        children: [
                          // NEXT OF KIN
                          Padding(
                            padding: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15,
                              top: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              onTap: () {
                                Get.to(() => NextOfKinScreen());
                              },
                              child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.screenWidth,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(
                                        Dimensions.radius20,
                                      )),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      left: Dimensions.width20,
                                      right: Dimensions.width15,
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/ic_nextofkin.png',
                                          width: Dimensions.width30 * 3,
                                          height: Dimensions.height30 * 3,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width45,
                                        ),
                                        BigText(
                                          text: "Next Of Kin",
                                          color: AppColors.black,
                                          size: Dimensions.font16,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),

                          // ADULTS ON THE FARM
                          Padding(
                            padding: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15,
                              top: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.screenWidth,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(
                                        Dimensions.radius20,
                                      )),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      left: Dimensions.width20,
                                      right: Dimensions.width15,
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/ic_adults.png',
                                          width: Dimensions.width30 * 3,
                                          height: Dimensions.height30 * 3,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width45,
                                        ),
                                        BigText(
                                          text: "Adults On The Farm",
                                          color: AppColors.black,
                                          size: Dimensions.font16,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),

                          // WORKING CONDITIONS
                          Padding(
                            padding: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15,
                              top: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.screenWidth,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(
                                        Dimensions.radius20,
                                      )),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      left: Dimensions.width20,
                                      right: Dimensions.width15,
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/ic_workingconditions.png',
                                          width: Dimensions.width30 * 3,
                                          height: Dimensions.height30 * 3,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width45,
                                        ),
                                        BigText(
                                          text: "Working Conditions",
                                          color: AppColors.black,
                                          size: Dimensions.font16,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),

                          // LIVING CONDITIONS
                          Padding(
                            padding: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15,
                              top: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.screenWidth,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(
                                        Dimensions.radius20,
                                      )),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      left: Dimensions.width20,
                                      right: Dimensions.width15,
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/ic_livingconditions.png',
                                          width: Dimensions.width30 * 3,
                                          height: Dimensions.height30 * 3,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width45,
                                        ),
                                        BigText(
                                          text: "Living Conditions",
                                          color: AppColors.black,
                                          size: Dimensions.font16,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),

                          // PERSONAL ASSETS
                          Padding(
                            padding: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15,
                              top: Dimensions.height10,
                            ),
                            child: GestureDetector(
                              child: Container(
                                  height: Dimensions.height30 * 4,
                                  width: Dimensions.screenWidth,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(
                                        Dimensions.radius20,
                                      )),
                                  child: Container(
                                    padding: EdgeInsets.only(
                                      left: Dimensions.width20,
                                      right: Dimensions.width15,
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/ic_personalassets.png',
                                          width: Dimensions.width30 * 3,
                                          height: Dimensions.height30 * 3,
                                        ),
                                        SizedBox(
                                          width: Dimensions.width45,
                                        ),
                                        BigText(
                                          text: "Personal Assets",
                                          color: AppColors.black,
                                          size: Dimensions.font16,
                                        ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}
