import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutterfarmcapture/mainpages/farmer/all_farmers.dart';
import 'package:flutterfarmcapture/resources/utils/colors.dart';
import 'package:flutterfarmcapture/resources/utils/dimensions.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/small_text.dart';

class FarmerDashboardScreen extends StatefulWidget {
  const FarmerDashboardScreen({super.key});

  @override
  State<FarmerDashboardScreen> createState() => _FarmerDashboardScreenState();
}

class _FarmerDashboardScreenState extends State<FarmerDashboardScreen> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    BigText(
                      text: "Manage Farmers",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: Dimensions.screenHeight,
          width: Dimensions.screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              SizedBox(height: Dimensions.height45,),
              
              GestureDetector(
                onTap: () {
                  Get.to(() => const AllFarmersScreen());
                },
                child: Column(
                  children: [
                    Container(
                      height: Dimensions.height30 * 4,
                      width: Dimensions.width30 * 4,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius:
                            BorderRadius.circular(Dimensions.radius30 * 5),
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/ic_1_1.png',
                              width: 60,
                              height: 60,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Farmers List",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                     SizedBox(
                      height: Dimensions.height10/2,
                    ),
                    SizedBox(
                      width: Dimensions.width45 + Dimensions.width45,
                      child: Divider()
                    ),
                    SmallText(
                      text: "View available farmers",
                      color: Colors.blueGrey,
                      size: Dimensions.font16 - 4,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height30 * 3,
              ),
              GestureDetector(
                onTap: () {},
                child: Column(
                  children: [
                    GestureDetector(
                      child: Container(
                        height: Dimensions.height30 * 4,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius30 * 5),
                        ),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.edit_note,
                                size: 60,
                                color: Colors.green,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    BigText(
                      text: "Add Farmer",
                      color: AppColors.black,
                      size: Dimensions.font16 - 4,
                    ),
                    SizedBox(
                      height: Dimensions.height10/2,
                    ),
                    Container(
                        width: Dimensions.width45 + Dimensions.width45,
                        child: Divider()),
                    SmallText(
                      text: "Add a new Farmer",
                      color: Colors.blueGrey,
                      size: Dimensions.font16 - 4,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
