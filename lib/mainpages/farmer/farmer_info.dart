
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../resources/utils/colors.dart';
import '../../resources/utils/dimensions.dart';
import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/small_text.dart';

class FarmerInfoScreen extends StatefulWidget {
  const FarmerInfoScreen({super.key});

  @override
  State<FarmerInfoScreen> createState() => _FarmerInfoScreenState();
}

class _FarmerInfoScreenState extends State<FarmerInfoScreen> {
  File? image;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    BigText(
                      text: "Additional Info",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: Dimensions.height20,
            bottom: Dimensions.height20,
          ),
          child: Column(
            children: [
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: Dimensions.height30 * 2,
                      width: Dimensions.height30 * 2,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              Dimensions.radius20 / 2),
                          color: AppColors.textGrey),
                      child: image != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(
                                  Dimensions.radius30 * 2),
                              child: Image.file(
                                image!,
                                fit: BoxFit.cover,
                              ))
                          : const Center(
                              child: Text('No Photo'),
                            ),
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BigText(
                          text: "Name: ",
                          color: AppColors.black,
                          size: Dimensions.font12,
                        ),
                        BigText(
                          text: "Name Here",
                          color: AppColors.mainColor,
                          size: Dimensions.font12,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Container(
                padding: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                ),
                child: Column(
                  children: [
                    Center(
                      child: DataTable(
                        border: TableBorder.all(color: AppColors.mainColor),
                        columns: [
                          DataColumn(
                            label: Center(
                              child: BigText(
                                text: "Personal Details",
                                color: AppColors.black,
                                size: Dimensions.font16,
                              ),
                            )
                          ),
                        ],
                        rows: [
                          DataRow(cells: [
                            DataCell(Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                SmallText(text: "Phone No : "),
                                SmallText(
                                  text: "076536727822",
                                  color: AppColors.mainColor,
                                ),
                              ],
                            )),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Marital Status : "),
                                  SmallText(
                                    text: "maritalStatus",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Gender : "),
                                  SmallText(
                                    text: "genderNmae",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "DOB : "),
                                  SmallText(
                                    text: "wardName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Bank : "),
                                  SmallText(
                                    text: "bankName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(
                            cells: [
                              DataCell(
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SmallText(text: "Bank Branch : "),
                                    SmallText(
                                      text: "bankBranchName",
                                      color: AppColors.mainColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DataRow(
                            cells: [
                              DataCell(
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SmallText(text: "Acount No : "),
                                    SmallText(
                                      text: "accNoController",
                                      color: AppColors.mainColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ]
                      ),
                    ),
                    SizedBox(height: Dimensions.height30,),
                    Center(
                      child: DataTable(
                        border: TableBorder.all(color: AppColors.mainColor),
                        columns: [
                          DataColumn(
                            label: BigText(
                              text: "Location Details",
                              color: AppColors.black,
                              size: Dimensions.font16,
                            )
                          ),
                        ],
                        rows: [
                          DataRow(cells: [
                            
                            DataCell(
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "County : "),
                                  SmallText(
                                    text: "countyName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "District : "),
                                  SmallText(
                                    text: "districtName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Town : "),
                                  SmallText(
                                    text: "wardName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Village : "),
                                  SmallText(
                                    text: "villageName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(cells: [
                            DataCell(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SmallText(text: "Collection Center : "),
                                  SmallText(
                                    text: "colCenterName",
                                    color: AppColors.mainColor,
                                  ),
                                ],
                              ),
                            ),
                          ]),
                          DataRow(
                            cells: [
                              DataCell(
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SmallText(text: "Nationality : "),
                                    SmallText(
                                      text: "nationalityName",
                                      color: AppColors.mainColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ]
                      ),
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}