import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutterfarmcapture/mainpages/farmer/additional_info/additional_info.dart';
import 'package:flutterfarmcapture/mainpages/farmer/all_farmers.dart';
import 'package:flutterfarmcapture/mainpages/farmer/edit_farmer.dart';
import 'package:flutterfarmcapture/resources/utils/colors.dart';
import 'package:flutterfarmcapture/resources/utils/dimensions.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../resources/widgets/big_text.dart';
import '../../resources/widgets/small_text.dart';
import 'farmer_info.dart';

class SingleFarmerDashboardScreen extends StatefulWidget {
  const SingleFarmerDashboardScreen({super.key});

  @override
  State<SingleFarmerDashboardScreen> createState() =>
      _SingleFarmerDashboardScreenState();
}

class _SingleFarmerDashboardScreenState
    extends State<SingleFarmerDashboardScreen> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Dimensions.height30 * 2),
        child: AppBar(
          backgroundColor: Colors.green,
          actions: [
            Container(
              width: Dimensions.screenWidth,
              height: Dimensions.height30 * 2,
              padding: EdgeInsets.only(
                left: Dimensions.width15,
                right: Dimensions.width15,
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(5, 5),
                      color: AppColors.gradientOne.withOpacity(0.1)),
                  BoxShadow(
                      blurRadius: 3,
                      offset: Offset(-5, -5),
                      color: AppColors.gradientOne.withOpacity(0.1))
                ],
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: Dimensions.iconSize24,
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    ),
                    BigText(
                      text: "Single Farmer Dashboard",
                      color: AppColors.textWhite,
                      size: Dimensions.font20,
                    ),
                    SizedBox(
                      width: Dimensions.width30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: Dimensions.screenHeight,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: Dimensions.height45,
              ),
              Center(
                child: BigText(
                  text: "Farmer Name",
                  color: AppColors.black,
                  size: Dimensions.font20,
                ),
              ),
              SizedBox(
                height: Dimensions.height30,
              ),
              Container(
                padding: EdgeInsets.only(
                  left: Dimensions.width45,
                  right: Dimensions.width45,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //individual profiling
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                openModeOfMappingDialog();
                              },
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_1.png',
                                        width: Dimensions.width30 * 2,
                                        height: Dimensions.height30 * 2,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Individual Profiling",
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SizedBox(
                              height: Dimensions.height10 / 2,
                            ),
                            SizedBox(
                                width: Dimensions.width45 * 2,
                                child: Divider()),
                            SmallText(
                              text: "Farmer's Profiling",
                              color: Colors.blueGrey,
                              size: Dimensions.font16 - 4,
                            )
                          ],
                        ),

                        //farmer agreement
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_2.png',
                                        width: Dimensions.width30 * 2,
                                        height: Dimensions.height30 * 2,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Farmer Agreement",
                              color: AppColors.black,
                              size: Dimensions.font16 - 4,
                            ),
                            SizedBox(
                              height: Dimensions.height10 / 2,
                            ),
                            SizedBox(
                                width: Dimensions.width45 * 2,
                                child: Divider()),
                            SmallText(
                              text: "Organization Agreement",
                              color: Colors.blueGrey,
                              size: Dimensions.font16 - 4,
                            )
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height45 * 2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //farmer inspection
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.to(() => AdditionalInfoScreen());
                              },
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_1_1.png',
                                        width: Dimensions.width30 * 2,
                                        height: Dimensions.height30 * 2,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Additional Information",
                              color: AppColors.black,
                              size: Dimensions.font14,
                            ),
                            SizedBox(
                              height: Dimensions.height10 / 2,
                            ),
                            SizedBox(
                                width: Dimensions.width45 * 2,
                                child: Divider()),
                            SmallText(
                              text: "Extra Information",
                              color: Colors.blueGrey,
                              size: Dimensions.font14,
                            )
                          ],
                        ),

                        //farm mapping
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () async {},
                              child: Container(
                                height: Dimensions.height30 * 4,
                                width: Dimensions.width30 * 4,
                                decoration: BoxDecoration(
                                  color: AppColors.textWhite,
                                  borderRadius: BorderRadius.circular(
                                      Dimensions.radius30 * 5),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/ic_2_3.png',
                                        width: Dimensions.width30 * 2,
                                        height: Dimensions.height30 * 2,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.height10,
                            ),
                            BigText(
                              text: "Farm Mapping",
                              color: AppColors.black,
                              size: Dimensions.font14,
                            ),
                            SizedBox(
                              height: Dimensions.height10 / 2,
                            ),
                            SizedBox(
                                width: Dimensions.width45 * 2,
                                child: Divider()),
                            SmallText(
                              text: "Mapping Farms",
                              color: Colors.blueGrey,
                              size: Dimensions.font14,
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // VIEW FARMER INFO OR EDIT PAGE
//dialog to choose mode of mapping
  Future<void> openModeOfMappingDialog() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: farmerInfoSelect(context),
          );
        });
  }

  farmerInfoSelect(context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(
            left: Dimensions.width20,
            top: Dimensions.height20,
            right: Dimensions.width20,
            bottom: Dimensions.height20,
          ),
          margin: EdgeInsets.only(top: Dimensions.height20),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Dimensions.radius30),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10)
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: Dimensions.height10,
              ),
              BigText(
                text: "Page Select",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              SmallText(
                text: "Would you like to ?",
                color: Colors.black,
                size: Dimensions.font16,
              ),
              SizedBox(
                height: Dimensions.height10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        Get.back();
                        Get.to(() => FarmerInfoScreen());
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "View Profile",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: Dimensions.width30 * 2,
                  ),
                  GestureDetector(
                      onTap: () {
                        Get.back();
                        Get.to(() => EditFarmer());
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 4,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Edit Profile",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: Dimensions.height20,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        height: Dimensions.height45,
                        width: Dimensions.width30 * 3,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius15 / 2)),
                        child: Center(
                          child: BigText(
                            text: "Cancel",
                            color: Colors.black,
                            size: Dimensions.font16,
                          ),
                        ),
                      ))),
            ],
          ),
        ),
        Positioned(
          left: Dimensions.width10 / 2,
          right: Dimensions.width10 / 2,
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: Dimensions.radius20,
              child: FaIcon(
                FontAwesomeIcons.circleQuestion,
                size: Dimensions.height45,
                color: Colors.green,
              )),
        ),
      ],
    );
  }
}
